/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(orderItemCount.count) AS minOrderItemCount,
       MAX(orderItemCount.count) AS maxOrderItemCount,
       ROUND(AVG(orderItemCount.count), 0) AS avgOrderItemCount
FROM (SELECT COUNT(`orderNumber`) as count FROM `orderdetails` GROUP BY `orderNumber`) AS orderItemCount
